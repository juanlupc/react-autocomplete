import countries from './countries.json';
import { FETCH_COUNTRIES } from './types';

const fetchCountries = () => (dispatch) => {
  dispatch({ type: FETCH_COUNTRIES, payload: countries });
};

module.exports = {
  fetchCountries,
};
