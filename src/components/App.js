import React, { Component } from 'react';
import { connect } from 'react-redux';
import AutoComplete from './AutoComplete';
import { fetchCountries } from '../actions';


class App extends Component {
  componentDidMount() {
    this.props.fetchCountries();
  }
  render() {
    return (
      <div className="container">
        <AutoComplete
          suggestions={this.props.suggestions}
        />
      </div>
    );
  }
}
function mapStateToProps({ autoComplete }) {
  return { suggestions: autoComplete };
}

export default connect(mapStateToProps, { fetchCountries })(App);
