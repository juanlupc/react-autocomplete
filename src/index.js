import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import App from './components/App';

const store = createStore(reducers, {}, applyMiddleware(reduxThunk, logger));

const AppM = () => (
  <Provider store={store}> 
      <App />
  </Provider>
);

ReactDOM.render(<AppM />, document.getElementById('root'));
